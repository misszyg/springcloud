package com.ams.common.result;
/**
 * @author：
 */
public interface IResultCode {

    String getCode();

    String getMsg();

}
