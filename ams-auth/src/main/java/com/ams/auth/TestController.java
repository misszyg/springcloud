package com.ams.auth;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：
 * @date: 2022/5/22
 */
@RestController
@RequestMapping("/test")
@AllArgsConstructor
@Slf4j
public class TestController {

    @GetMapping("/public-test")
    public String test() {
        return "ams-auth 为您提供服务";
    }
}
