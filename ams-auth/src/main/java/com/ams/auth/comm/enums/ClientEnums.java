package com.ams.auth.comm.enums;

import lombok.Getter;

/**
 * @author：
 * @date: 2022/5/23
 */
public enum ClientEnums {
    MEMBER_CLIENT("member","会员客户端"),
    ADMIN_CLIENT("ams","后台客户端");

    @Getter
    private String name;
    @Getter
    private String desc;
    ClientEnums(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }
}
