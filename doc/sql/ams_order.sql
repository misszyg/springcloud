/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.64.6
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 192.168.64.6:3306
 Source Schema         : ams_order

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 21/12/2021 21:49:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for demo_order
-- ----------------------------
DROP TABLE IF EXISTS `demo_order`;
CREATE TABLE `demo_order` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) DEFAULT NULL COMMENT '用户id',
  `product_id` bigint(11) DEFAULT NULL COMMENT '产品id',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `money` decimal(11,0) DEFAULT NULL COMMENT '金额',
  `status` int(1) DEFAULT NULL COMMENT '订单状态：0：创建中；1：已完结',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1473036927172104194 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_order
-- ----------------------------
BEGIN;
INSERT INTO `demo_order` VALUES (13, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (14, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (15, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (16, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (17, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (1472698413381357569, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (1472698929159114754, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (1472699534942470146, 1, 1, 10, 100, 1, NULL);
INSERT INTO `demo_order` VALUES (1473036927172104193, 1, 1, 10, 100, 1, NULL);
COMMIT;

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log` (
  `branch_id` bigint(20) NOT NULL COMMENT 'branch transaction id',
  `xid` varchar(128) NOT NULL COMMENT 'global transaction id',
  `context` varchar(128) NOT NULL COMMENT 'undo_log context,such as serialization',
  `rollback_info` longblob NOT NULL COMMENT 'rollback info',
  `log_status` int(11) NOT NULL COMMENT '0:normal status,1:defense status',
  `log_created` datetime(6) NOT NULL COMMENT 'create datetime',
  `log_modified` datetime(6) NOT NULL COMMENT 'modify datetime',
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AT transaction mode undo table';

-- ----------------------------
-- Records of undo_log
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
