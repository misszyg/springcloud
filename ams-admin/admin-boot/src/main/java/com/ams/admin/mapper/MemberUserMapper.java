package com.ams.admin.mapper;

import com.ams.admin.dto.MemberUserAuthDTO;
import com.ams.admin.pojo.entity.MemberUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author：
 */
@Mapper
public interface MemberUserMapper extends BaseMapper<MemberUser> {

    MemberUserAuthDTO getByUsername(@Param("userName") String userName);
}
