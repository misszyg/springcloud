package com.ams.admin.mapper;

import com.ams.admin.pojo.entity.SysOauthClient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author：
 */
@Mapper
public interface SysOauthClientMapper extends BaseMapper<SysOauthClient> {
}
