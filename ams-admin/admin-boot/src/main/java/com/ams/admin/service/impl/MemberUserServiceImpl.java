package com.ams.admin.service.impl;

import com.ams.admin.dto.MemberUserAuthDTO;
import com.ams.admin.mapper.MemberUserMapper;
import com.ams.admin.pojo.entity.MemberUser;
import com.ams.admin.service.IMemberUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Service
@RequiredArgsConstructor
public class MemberUserServiceImpl extends ServiceImpl<MemberUserMapper, MemberUser> implements IMemberUserService {
    @Override
    public MemberUserAuthDTO getByUsername(String username) {
        MemberUserAuthDTO memberUserAuthDTO = this.baseMapper.getByUsername(username);
        return memberUserAuthDTO;
    }

}
