package com.ams.admin.service;

import com.ams.admin.pojo.entity.SysOauthClient;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author：
 */
public interface ISysOauthClientService extends IService<SysOauthClient> {
}
