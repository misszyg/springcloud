package com.ams.admin.service.impl;

import com.ams.admin.mapper.SysOauthClientMapper;
import com.ams.admin.pojo.entity.SysOauthClient;
import com.ams.admin.service.ISysOauthClientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Service
@RequiredArgsConstructor
public class SysOauthClientServiceImpl extends ServiceImpl<SysOauthClientMapper, SysOauthClient> implements ISysOauthClientService {

}
