package com.ams.admin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：
 * @date: 2022/5/22
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {
//
//    @Value("${activate}")
//    private String activate;
//
//    @GetMapping("/public-test")
//    public String test() {
//        return "ams-admin 为您提供服务，当前环境为"+activate;
//    }
}
