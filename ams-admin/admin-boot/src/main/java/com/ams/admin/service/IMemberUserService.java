package com.ams.admin.service;


import com.ams.admin.dto.MemberUserAuthDTO;
import com.ams.admin.pojo.entity.MemberUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author：
 */
public interface IMemberUserService extends IService<MemberUser> {


    /**
     * 根据用户名获取认证用户信息，携带角色和密码
     *
     * @param username
     * @return
     */
    MemberUserAuthDTO getByUsername(String username);

}
